from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import time
import uuid
import os
import glob
import math
import logging
import numpy as np
from os.path import join
import cv2
import argparse
import torch
from torch import nn
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo
from NSFW_BOX.models.networks.pose_dla_dcn import get_pose_net, load_model, pre_process, ctdet_decode, post_process, \
    merge_outputs
from PIL import Image
import cv2
import numpy as np

class BOX_MODEL(object):

    def __init__(self):
        self.num_layers = 34
        self.heads = {'hm': 1, 'wh': 2, 'reg': 2}
        self.head_conv = 256
        self.scale = 1.0
        self.threshold = 0.6
        self.num_classes = 1
        self.model = get_pose_net(num_layers=self.num_layers, heads=self.heads, head_conv=self.head_conv)
        self.model = load_model(self.model, "weights_box.pth")
        self.device = torch.device("cuda:0")
        self.model.to(self.device)
        self.model.eval()

    def predict(self, img):
        image, meta = pre_process(img, self.scale)
        image = image.to(self.device)
        with torch.no_grad():
            output = self.model(image)[-1]
            hm = output['hm'].sigmoid_()
            wh = output['wh']
            reg = output['reg']
            dets = ctdet_decode(hm, wh, reg=reg, K=100)
        dets = post_process(dets, meta)
        detections = [dets]
        results = merge_outputs(detections)
        list_result = []
        for j in range(1, self.num_classes + 1):
            for bbox in results[1]:
                if (bbox[4] >= self.threshold):
                    xmin, ymin, xmax, ymax = max(int(bbox[0]), 0), max(0, int(bbox[1])), min(int(bbox[2]),
                                                                                             img.shape[1]), min(
                        int(bbox[3]), img.shape[0])
                    list_result.append(img[ymin:ymax, xmin:xmax, :])
                    #list_result.append(Image.fromarray(cv2.cvtColor(img[ymin:ymax, xmin:xmax], cv2.COLOR_BGR2RGB)))
        return list_result
