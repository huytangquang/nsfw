import numpy as np
from keras import applications
from keras.layers import Dropout, Flatten, Dense, Input
from keras.models import Model
import cv2
import tensorflow as tf
from keras import backend as K
from PIL import Image

mapping = {
    0: '16+',
    1: '18+',
    2: 'binh_thuong',
}

class NSFWClassification(object):
    def __init__(self, threshold=0.0):
        self.threshold = threshold
        self.graph = tf.Graph()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.15
        self.sess = tf.Session(graph=self.graph, config=config)
        with self.graph.as_default():
            with self.sess.as_default():
                self.create_model()

    def create_model(self, input_shape=(224, 224, 3), output_classes=3):
        base_model = applications.MobileNet(weights='imagenet', include_top=False, input_shape=input_shape)
        model_inputs = base_model.input
        common_inputs = base_model.output
        x = Flatten()(common_inputs)
        x = Dense(256, activation='tanh')(x)#256
        x = Dropout(0.5)(x)
        predictions_class = Dense(output_classes, activation='softmax', name='predictions_class')(x)
        ## Create Model
        model = Model(inputs=model_inputs, outputs=predictions_class)
        model.load_weights("weights_step1.hdf5", by_name=True)
        self.model = model

    def preprocess_image(self, img):
        img = cv2.resize(img, (224, 224))
        return img

    def predict_batch(self, images):
        batch_img = []
        for img in images:
            img = cv2.resize(img, (224, 224))
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            batch_img.append(img)
        batch_img = np.asarray(batch_img)
        with self.graph.as_default():
            with self.sess.as_default():
                result = self.model.predict(batch_img)
        result_return = []
        for rs in result:
            idx = np.argmax(rs)
            if rs[idx] > self.threshold:
                result_return.append(mapping[idx])
            else:
                result_return.append("binh_thuong")
        return result_return

    def predict_single(self, img):
        tf.keras.backend.set_session(self.sess)
        img = self.preprocess_image(img)
        img = np.resize(img, (1, 224, 224, 3))
        with self.graph.as_default():
            with self.sess.as_default():
                result = self.model.predict(img)[0]
        labels = []
        idx_max = np.argmax(result)
        if result[idx_max] > self.threshold:
            labels.append(mapping[idx_max])
        else:
            labels.append(mapping[2])
        return labels[0], result[idx_max]
