from random import choice
from flask import Flask, jsonify, request
import time, requests
from PIL import Image
from io import BytesIO
from flask import send_file
import io
import uuid
import os
from model_box_classify import MODEL_BOX_CLASSIFY
import cv2
import numpy as np

model = MODEL_BOX_CLASSIFY()

desktop_agents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']


def random_headers():
    return {'User-Agent': choice(desktop_agents),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def download_image(image_url):
    header = random_headers()

    response = requests.get(image_url, headers=header, stream=True, verify=False, timeout=5)

    image = Image.open(BytesIO(response.content)).convert('RGB')

    return image

def jsonify_str(output_list):
    with app.app_context():
        with app.test_request_context():
            result = jsonify(output_list)
    return result

app = Flask(__name__)
def create_query_result(input_url, results, error=None):
    if error is not None:
        results = 'Error: ' + str(error)
    query_result = {
        'results': results
    }
    return query_result

@app.route("/query", methods=['GET', 'POST'])
def query():
    if request.method == "POST":
        data = request.files["file"].read()
        type = request.args.get('type', default='1', type=str)
        image_url = "Upload"
        try:
            img = Image.open(BytesIO(data)).convert('RGB')
        except Exception as ex:
            print(ex)
            return jsonify(create_query_result("Upload", "upload error"))
    else:
        try:
            image_url = request.args.get('url', default='', type=str)
            img = download_image(image_url)
            img = img.convert('RGB')
        except Exception as ex:
            return jsonify_str(create_query_result("", "", ex))
    img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    start = time.time()
    result_json = {"result" : model.predict(img), "time is" : str(time.time() - start)}
    print(result_json)
    #print("Time is: ", time.time() - start)
    if result_json is None:
        return jsonify_str(create_query_result("", "", "No"))

    return jsonify_str(result_json)
app.run("172.26.33.21", 1590, threaded=False, debug=False)
