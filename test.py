import cv2
from model_box_classify import MODEL_BOX_CLASSIFY
import glob
import os

folder = "../test_nsfw/16+/"
folder_save = "../test_nsfw/16+pred_bt_old/"

model = MODEL_BOX_CLASSIFY()

list_img = glob.glob(os.path.join(folder, "*"))

for index, link in enumerate(list_img):
    #print(index)
    img = cv2.imread(link)
    rs = model.predict(img)
    print(rs)
    if rs != "binh_thuong":
        cv2.imwrite(os.path.join(folder_save, os.path.basename(link)), img)

