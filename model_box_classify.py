from NSFW_BOX.model_classify import NSFW_MODEL
from NSFW_BOX.model_box import BOX_MODEL
from NsfwClf.nsfw_model import NSFWClassification



class MODEL_BOX_CLASSIFY(object):

    def __init__(self):
        self.nsfw_clf = NSFWClassification()
        self.model_box = BOX_MODEL()
        self.model_classify = NSFW_MODEL()
        print("Done load model")

    def predict(self, img):
        result_dict = {}
        clf_label, score = self.nsfw_clf.predict_single(img)
        print("step 1:", clf_label, score)
        if score > 0.85:
            return {"step1" : clf_label, "score-step1" : str(score)}
        list_img = self.model_box.predict(img)
        print("total person: ", len(list_img))
        if len(list_img) > 0:
            rs, box_score = self.model_classify.predict_batch(list_img)
            return {"step1" : clf_label, "score-step1" : str(score), "step2" : rs, "score_step2" : str(box_score)}
        else:
            return {"step1" : clf_label, "score-step1" : str(score), result_final : "binh_thuong", "score_step2" : str(box_score)}
